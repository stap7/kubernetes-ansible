#!/bin/sh
nodeip=$1
sed_pattern="s/#newip/${nodeip}/g"
sed ${sed_pattern} inventories/hosts > inventories/host_new
ansible-playbook -i inventories/host_new removal_node.yml
rm inventories/host_new
echo Done
