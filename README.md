# Kubernetes-ansible project

This project setups kubernetes cluster to virtual servers (10.10.10.10 and 10.10.10.11) and deploys dev tools.
Also, a removal mechanism is included.

### Installing

In order to setup K8s cluster and all deployments:

```
./deploy-all.sh
```

And if you want to remove:

```
./remove-all.sh
```

### Add/Remove nodes to Kubernetes

In order to add a new node in the cluster :
```
./add-node.sh <hostip>
```

To remove a node from the cluster:
```
./remove-node.sh <hostip>
```