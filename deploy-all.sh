#!/bin/sh

ansible-playbook -i inventories/hosts playbook.yml

rm -rf ~/.kube && scp -r root@10.10.10.10:/root/.kube/ ~/

kubectl get nodes

kubectl create namespace tools
kubectl create namespace tools

kubectl apply -f deployments/k8dashboard
kubectl apply -f deployments/grafana
kubectl apply -f deployments/sinopia
kubectl apply -f deployments/sourcegraph
kubectl apply -f deployments/ztp-apache
kubectl apply -f deployments/metrics-server

kubectl apply -f deployments/nginx-ingress/common/ns-sa.yaml
kubectl apply -f deployments/nginx-ingress/common/default-server-secret.yaml
kubectl apply -f deployments/nginx-ingress/common/nginx-config.yaml
kubectl apply -f deployments/nginx-ingress/rbac/rbac.yaml
kubectl apply -f deployments/nginx-ingress/deployment/deployment-nginx-ingress.yaml
kubectl apply -f deployments/nginx-ingress/deployment/svc-nginx-ingress.yaml

echo Done
